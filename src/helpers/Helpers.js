import React, { useState } from 'react'

const request = (token, url, method, body, headers) => {

    let newData = body
    newData = JSON.stringify(body)
    
    let requestHeaders = {
        'Content-Type': 'application/json',
        "authorization": token
    }

    return fetch(url, {
            method,
            body: newData,
            headers: {
                ...requestHeaders
            }
        }).then(response => response.json())
}   

export default request

