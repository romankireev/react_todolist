import React, {useState} from 'react'
import ModalForm from '../Modal/ModalForm'
import styles from './UserInfo.module.css'

const UserInfo = ( {getProfileInfo, editPassword, editUserInfo, userInfoParams,
                    passwordParams, isPasswordChange, setIsPasswordChange,
                    isPasswordError, setIsPasswordError, handlerUserInfoParams, handlerPasswordParams} ) => {
    
    const [modalActive, setModalActive] = useState(false)

    const handlerUserInfo = () => {
        setModalActive(true)
        getProfileInfo()
    }

    return (
        <div className={styles['user-info-form']}>
            <div className={styles['user-info-panel']}>
                <button className={styles['user-info-btn']} onClick={handlerUserInfo}>Информация о пользователе</button>
                <ModalForm active={modalActive} 
                           setActive={setModalActive}
                           userInfoParams={userInfoParams}
                           passwordParams={passwordParams}
                           editPassword={editPassword}
                           editUserInfo={editUserInfo}
                           isPasswordChange={isPasswordChange}
                           setIsPasswordChange={setIsPasswordChange}
                           isPasswordError={isPasswordError}
                           setIsPasswordError={setIsPasswordError}
                           handlerUserInfoParams={handlerUserInfoParams}
                           handlerPasswordParams={handlerPasswordParams}
                           />
            </div>
        </div> 
    )
}

export default UserInfo