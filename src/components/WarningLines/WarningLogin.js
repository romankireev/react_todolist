import { func } from 'prop-types'
import React from 'react'
import styles from './WarningLine.module.css'

function WarningLineLogin() {
    return (
        <div className={styles['warning-line']}>Указанные учетные данные пользователя не найдены. Чтобы зарегистрировать пользователя под указанными учетными данными, нажмите "Зарегистрироваться"</div>
    )
}

export default WarningLineLogin