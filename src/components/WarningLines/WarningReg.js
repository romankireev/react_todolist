import React from 'react'
import styles from './WarningLine.module.css'

function WarningLineReg() {
    return (
        <div className={styles['warning-line']}>Такой пользователь уже зарегистрирован. Для входа под указанными учетными данными нажмите кнопку "Войти"</div>
    )
}

export default WarningLineReg