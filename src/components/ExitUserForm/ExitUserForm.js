import { func } from 'prop-types'
import React from 'react'
import styles from './ExitUserForm.module.css'

function ExitUserForm( {onExit} ) {

    return (
        <>
            <div className={styles['exit-user-form']}>
                <div className={styles['exit-user-panel']}>
                <button className={styles['exit-user-btn']} 
                        type='submit'
                        onClick={onExit}>Выйти из профиля</button>
                </div>
            </div>
        </>
    )
}

export default ExitUserForm