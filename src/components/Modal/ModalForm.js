import React, { useState } from 'react'
import './ModalForm.css'
import ModalContent from './ModalContent'

const ModalForm = ( {active, setActive, isTodoItemModal, todoStatus, handlerTodoItemStatus, userInfoParams, passwordParams,
                    handlerUserInfoParams, handlerPasswordParams, editUserInfo,  
                    editPassword, isPasswordChange, setIsPasswordChange, isPasswordError, setIsPasswordError} ) => {

    const [isEdit, setIsEdit] = useState(false)

    const modalFormOnClick = () => {
        setActive(false)
        setIsPasswordChange(false)
        setIsPasswordError(false)
    }

    const editUserInfoOnClick = () => {
        editUserInfo(userInfoParams.emailText, userInfoParams.userNameText, userInfoParams.userAgeText)
        setIsEdit(false)
    }

    const editPasswordOnClick = () => {
        editPassword(passwordParams.oldPasswordText, passwordParams.newPasswordText)
    }

    const handlerEditUserForm = () => {
        setIsEdit(true)
    }

    const handlerTodoStatus = (status) => {
        todoStatus = status
        setActive(false)
        handlerTodoItemStatus()
    }
    
    return (
        <div className={active ? 'modal active' : 'modal'} onClick={modalFormOnClick}>
            <div className='modal-content' onClick={e => e.stopPropagation()}>
                <ModalContent handlerUserInfoParams={handlerUserInfoParams}
                              handlerPasswordParams={handlerPasswordParams}
                children={ !isTodoItemModal ?
                <>
                <div className='modal-content-wrapper'>
                    <div className='modal-info-btn-form'>
                        {!isEdit ? <button className='edit-info-btn' 
                                            onClick={handlerEditUserForm}>Редактировать</button> : <></> }
                    </div>
                    <div className='modal-content-info-form'>
                        <div className='email-form'>
                            <p className='userinfo-title'>Вы вошли как:</p>
                            <input className='userinfo-input' 
                                                placeholder={userInfoParams?.emailText}
                                                value={userInfoParams?.emailText}
                                                onChange={email => handlerUserInfoParams(email.target.value, userInfoParams?.userNameText, userInfoParams?.userAgeText)}
                                                readOnly={!isEdit ? true : false} />  
                        </div>
                        <div className='username-form'>
                            <p className='userinfo-title'>Имя</p>
                            <input className='userinfo-input' 
                                                placeholder={userInfoParams?.userNameText}
                                                value={userInfoParams?.userNameText}
                                                onChange={name => handlerUserInfoParams(userInfoParams?.emailText, name.target.value, userInfoParams?.userAgeText)}
                                                readOnly={!isEdit ? true : false} />                  
                        </div>
                        <div className='userage-form'>
                            <p className='userinfo-title'>Возраст</p>
                            <input className='userinfo-input' 
                                                placeholder={userInfoParams?.userAgeText}
                                                value={userInfoParams?.userAgeText}
                                                onChange={age => handlerUserInfoParams(userInfoParams?.emailText, userInfoParams?.userNameText, age.target.value)}
                                                readOnly={!isEdit ? true : false}/>  
                        </div>
                    </div>
                    <div className='save-user-info-btn-form'>
                        {isEdit ? <button className='save-user-info-btn' onClick={editUserInfoOnClick}>Сохранить</button> : <></> }
                    </div>
                    <div className='edit-password-form'>
                        <p className='edit-user-info-title'>Изменить пароль</p>
                        <input className='edit-user-info-input' 
                                placeholder='Введите текущий пароль'
                                value={passwordParams?.oldPasswordText} 
                                onChange={oldpassword => handlerPasswordParams(oldpassword.target.value, passwordParams?.newPasswordText)}/>
                        <input className='edit-user-info-input' 
                                placeholder='Введите новый пароль'
                                value={passwordParams?.newPasswordText}
                                onChange={newpassword => handlerPasswordParams(passwordParams?.oldPasswordText, newpassword.target.value)} />
                        <button className='save-user-info-btn' onClick={editPasswordOnClick}>Сохранить</button>
                        {isPasswordChange ? <div className='pass-edit-complete-line'>Пароль успешно изменен</div> : <></>}
                        {isPasswordError ? <div className='pass-edit-error-line'>Вы ввели неверный текущий пароль или введенный новый пароль совпадает с текущим. Повторите попытку</div> : <></>}
                    </div>
                </div>
                </>
                : <><div className='todo-item-modal-form'>
                        <p className='todo-item-modal-title'>Вы уверены, что хотите отменить выполнение задачи?</p>
                        <div className='todo-item-modal-btns-form'>
                            <button className='todo-item-yes-btn' onClick={() => handlerTodoStatus('PENDING')}>Да</button>
                            <button className='todo-item-no-btn' onClick={() => setActive(false)}>Нет</button>
                        </div>
                    </div>
                    </>                  
                } />
            </div>
        </div>
    )
}

export default ModalForm

