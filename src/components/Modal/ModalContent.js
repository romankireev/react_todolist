import React from 'react'
import './ModalContent.css'

const ModalContent = ( props ) => {

    return (
        <>
            {props.children}
        </>
    )
    
}

export default ModalContent