import React from 'react'
import styles from './AuthForm.module.css'


const AuthForm = ( {regFunction, logingFunction, handlerAuthParams, login, password}) => {

    return (
        <div className='auth-form'>
            <input className={styles["auth-input"]} 
                    type='email' 
                    placeholder='Логин'
                    value={login}
                    onChange={email => handlerAuthParams(email.target.value, password)} />
            <input className={styles["auth-input"]} 
                    type='text' 
                    placeholder='Пароль'
                    value={password}
                    onChange={password => handlerAuthParams(login, password.target.value)} />
            <div className='auth-form-buttons'>
                <button className={styles["auth-btn"]} 
                        type='submit'
                        onClick={() => regFunction()}>Зарегистрироваться</button>
                <button className={styles["auth-btn"]} 
                        type='submit' 
                        onClick={() => logingFunction()}>Войти</button>
            </div>
        </div>
    )
}

export default AuthForm