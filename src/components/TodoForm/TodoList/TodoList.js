import React from 'react'
import TodoListItem from '../TodoItem/TodoListItem'


function TodoList( {todoArray, removeTodoItem, doneTodoItem} ) {

    return (
        <>
            <ul className='todo-list'>
                {todoArray.map((todo) => {
                     return <TodoListItem todo={todo} 
                                          key={todo.id}
                                          removeTodoItem={removeTodoItem} 
                                          doneTodoItem={doneTodoItem} />
                })} 
            </ul>
        </>
    )
}

export default TodoList