import React, { useState } from 'react'
import styles from './TodoInputForm.module.css'
import TodoList from '../TodoList/TodoList'

   
function TodoInputForm ( {todoArray, addTodoItem, removeTodoItem, doneTodoItem}) {

    const [todoInputValue, setTodoInputValue] = useState('')

    const handlerTodo = (event) => {
        addTodoItem(todoInputValue)
        setTodoInputValue('')
    }

    return (
        <>
        <div className='todo-input-form' >
            <input className={styles["todo-input"]} 
                    type='text' 
                    placeholder='Введите дело' 
                    value={todoInputValue}
                    onChange={ event => setTodoInputValue(event.target.value)} />
            <button className={styles["todo-add-button"]} type='submit' onClick={handlerTodo}>Добавить</button>
        </div>
        <TodoList todoArray={todoArray}
                  removeTodoItem={removeTodoItem}
                  doneTodoItem={doneTodoItem} />
        </>
    )
}



export default TodoInputForm