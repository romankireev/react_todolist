import React, {useState} from 'react'
import './TodoListItem.css'
import ModalForm from '../../Modal/ModalForm'

function TodoListItem( {todo, removeTodoItem, doneTodoItem} ) {

    const [modalActive, setModalActive] = useState(false)
    const [isTodoItemModal, setIsTodoItemModal] = useState(false)
    const todoStatus = todo.status

    const handlerTodoItemModal = () => {
        setModalActive(true)
        setIsTodoItemModal(true)
    }

    const handlerTodoItemStatus = () => {
        doneTodoItem(todo.id, todo.status)
    }

    return (
        <>
        <li className={todo.status === 'DONE' ? 'todo-list-item li-clicked' : 'todo-list-item'} >
                {todo.body}
            <div className="li-btns">
                <button className="del-btn" type='submit' 
                        onClick={() => removeTodoItem(todo.id)}>Удалить</button>
                <button className={todo.status === 'DONE' ? 'done-btn-active' : "done-btn"} type='submit' 
                        onClick={todo.status === 'DONE' ? 
                        handlerTodoItemModal
                        : () => doneTodoItem(todo.id, todo.status)}>{todo.status === 'DONE' ? 'Отменить выполнение' : 'Выполнить'}</button>
            </div>
        </li>
        <ModalForm active={modalActive} 
                   setActive={setModalActive} 
                   isTodoItemModal={isTodoItemModal}
                   todoStatus={todoStatus}
                   handlerTodoItemStatus={handlerTodoItemStatus}  />
        </>
    )
}

export default TodoListItem

// return <Mod activee={modalActivee} 
//                     setActivee={setModalActivee}

