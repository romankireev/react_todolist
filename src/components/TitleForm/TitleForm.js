import React from 'react'
import styles from './TitleForm.module.css'
import pencil from '../../img/pencil.png'

function TitleForm () {
    return (
        <div className={styles["title-div"]}>
            <h1 className={styles.h1}>Список дел</h1>
            <img src={pencil} alt='pencil' className={styles['pencil-img']}/>
        </div>
    )
}

export default TitleForm