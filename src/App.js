import React, {useEffect, useState} from 'react'
import AuthForm from './components/AuthForm/AuthForm'
import TodoInputForm from './components/TodoForm/TodoInputForm/TodoInputForm'
import TitleForm from './components/TitleForm/TitleForm'
import ExitUserForm from './components/ExitUserForm/ExitUserForm'
import WarningLogin from './components/WarningLines/WarningLogin'
import WarningReg from './components/WarningLines/WarningReg'
import UserInfo from './components/UserInfo/UserInfo'
import request from './helpers/Helpers'

const App = () => {

  const [authParams, setAuthParams] = useState({
    email: '',
    password: ''
  })
  
  const [tokenAuth, setTokenAuth] = useState('')
  const [isAuth, setIsAuth] = useState(false)

  const [todoArray, setTodoArray] = useState([])
  
  const [isRegError, setIsRegError] = useState(false)
  const [isLoginError, setIsLoginError] = useState(false)

  const [isPasswordChange, setIsPasswordChange] = useState(false)
  const [isPasswordError, setIsPasswordError] = useState(false)

  const [userInfoParams, setUserInfoParams] = useState({
    emailText: '',
    userAgeText: '',
    userNameText: ''
  })

  const [passwordParams, setPasswordParams] = useState({
    oldPasswordText: '',
    newPasswordText: ''
  })

  useEffect(() => {
    const token = localStorage.getItem("Token")
    if (token && token !== tokenAuth) {
      setIsAuth(true)
      setTokenAuth(token)
    }
  }, [])

  useEffect(() => {
    if (tokenAuth) {
      getProfileInfo()
      getTodos()
      setIsPasswordChange(false)
      setIsPasswordError(false)
    } else {
      setIsRegError(false)
      setIsLoginError(false)
    }
  }, [tokenAuth])

  
  const logInUp = () => {
    request(tokenAuth, `http://localhost:3005/auth/sign-in`, "POST", {   
      email: authParams.email,
      password: authParams.password, 
    }).then(response => {
      if (response.statusCode !== 400) {
          setTokenAuth(response.token)
          localStorage.setItem("Token", response.token)
          setIsAuth(true)
          setAuthParams( {
            email: '',
            password: ''
          })
        } else if (response.statusCode === 400) {
            setIsRegError(false)
            setIsLoginError(true)
        }
      })
  }

  const regUp = () => {
    request(tokenAuth, `http://localhost:3005/auth/sign-up`, "POST", {   
      email: authParams.email,
      password: authParams.password, 
    }).then(response => {
      if (response.statusCode !== 400) {
          setTokenAuth(response.token)
          localStorage.setItem("Token", response.token)
          setIsAuth(true)
          setAuthParams( {
            email: '',
            password: ''
          })
        } else if (response.statusCode === 400) {
            setIsRegError(true)
            setIsLoginError(false)
          }
      })
  }

  const getTodos = () => {
    request(tokenAuth, 'http://localhost:3005/todo/list', "GET")
      .then(todoArray => {
          setTodoArray(todoArray)
      })
  }

  const logOut = () => {
    request(tokenAuth, `http://localhost:3005/auth/sign-out`, "POST")
      .then(response => {
          localStorage.removeItem("Token")
          setIsAuth(false)
          setIsRegError(false)
          setIsLoginError(false)
      })
  }

  const addTodoItem = (body) => {
    request(tokenAuth, "http://localhost:3005/todo", "POST", {
      body: body
    }).then(response => {
          setTodoArray(todoArray => {
            return [
              ...todoArray,
              response
            ]
          }) 
      })
  }

  const removeTodoItem = (id) => {
    request(tokenAuth, `http://localhost:3005/todo/${id}`, "DELETE")
      .then(response => {
          setTodoArray(todoArray.filter(todo => todo.id !== id))
      })
  }

  const doneTodoItem = (id, status, body) => {

    let oldStatus = 'PENDING'

    if (status === 'PENDING') {
      oldStatus = 'DONE'
    } else {
      oldStatus = 'PENDING'
    }

    request(tokenAuth, `http://localhost:3005/todo/${id}`, "PATCH", {   
        status: oldStatus,
        body: body 
    }).then(response => {
        setTodoArray(todoArray.map(todo => {
          if (todo.id === id) {
            if (todo.status === 'PENDING') {
              todo.status = 'DONE'
            } else if (todo.status === 'DONE') {
              todo.status = 'PENDING'
            }
          }
          return todo
        }))
    })
  }

  const getProfileInfo = () => {
    request(tokenAuth, 'http://localhost:3005/profile/me', "GET")
      .then(response => {
        setUserInfoParams({
          emailText: response.email,
          userNameText: response.name,
          userAgeText: response.age
        })
      })
  }

  const editUserInfo = (email, username, userage) => {
    request(tokenAuth, 'http://localhost:3005/profile/me', "PATCH", {   
        email: email,
        name: username,
        age: userage  
    }).then(response => {
      setUserInfoParams({
        emailText: response.email,
        userNameText: response.name,
        userAgeText: response.age
      })
    })
  }

  const editPassword = (oldPassword, newPassword) => {
    request(tokenAuth, 'http://localhost:3005/profile/me/password', "PATCH", { 
        oldPassword: oldPassword,
        newPassword: newPassword
    }).then(response => {
      if (response.statusCode !== 400) {
        setPasswordParams({
          oldPasswordText: response.oldPassword,
          newPasswordText: response.newPassword
        })
        setIsPasswordChange(true)
        setIsPasswordError(false)
        setPasswordParams({
          oldPasswordText: '',
          newPasswordText: ''
        })
      } else if (response.statusCode === 400) {
        setIsPasswordError(true)
        setIsPasswordChange(false)
      } 
    })
  }

  return (
      <div className='wrapper'>
      {
        !isAuth
        ? ( <>
              <TitleForm />
              <AuthForm 
              login={authParams.email} 
              password={authParams.password}  
              regFunction={regUp}
              logingFunction={logInUp}
              handlerAuthParams={(email, password) => setAuthParams(s => ({
                ...s,
                email: email,
                password: password
              })) }/>
              { isRegError ? <WarningReg /> : <></> }
              { isLoginError ? <WarningLogin /> : <></> }
            </>
        )
        : ( <>
              <UserInfo getProfileInfo={getProfileInfo}
                        editUserInfo={editUserInfo}
                        editPassword={editPassword}
                        userInfoParams={userInfoParams}
                        passwordParams={passwordParams}
                        isPasswordChange={isPasswordChange}
                        setIsPasswordChange={setIsPasswordChange}
                        isPasswordError={isPasswordError}
                        setIsPasswordError={setIsPasswordError}
                        handlerUserInfoParams={(email, name, age) => setUserInfoParams( e => ({
                          ...e,
                          emailText: email,
                          userNameText: name,
                          userAgeText: age
                        })) }
                        handlerPasswordParams={(oldpassword, newpassword) => setPasswordParams(e => ({
                          ...e,
                          oldPasswordText: oldpassword,
                          newPasswordText: newpassword
                        })) }
                        />
              <ExitUserForm onExit={logOut} />
              <TitleForm />
                <div>
                  <TodoInputForm todoArray={todoArray}
                                 addTodoItem={addTodoItem}
                                 removeTodoItem={removeTodoItem}
                                 doneTodoItem={doneTodoItem}
                                 />
                </div>
            </> )
      }
      </div>
  )
}

export default App



